import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Konverter {

    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.0.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);


        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement el3 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        el3.click();
        MobileElement el4 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el4.click();
        MobileElement el5 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el5.click();
        MobileElement el6 = ad.findElementByAccessibilityId("Picker-Konverter-Jenis1");
        el6.click();
        MobileElement el7 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]");
        el7.click();
        MobileElement el8 = ad.findElementByAccessibilityId("TextInput-Konverter-Value");
        el8.sendKeys("2");
        ad.hideKeyboard();
        MobileElement e19 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.EditText");
        Assert.assertEquals(e19.getText(),"1.199.296");
        MobileElement e20 = ad.findElementByAccessibilityId("Picker-Konverter-Jenis1");
        e20.click();
        MobileElement e21 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[3]");
        e21.click();
        MobileElement e22 = ad.findElementByAccessibilityId("TextInput-Konverter-Value");
        e22.sendKeys("50000");
        ad.hideKeyboard();
        MobileElement e23 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.EditText");
        Assert.assertEquals(e23.getText(),"12.5 Dirham");
        MobileElement e24 = ad.findElementByAccessibilityId("Picker-Konverter-Jenis1");
        e24.click();
        MobileElement e25 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[4]");
        e25.click();
        MobileElement e26 = ad.findElementByAccessibilityId("TextInput-Konverter-Value");
        e26.sendKeys("5");
        ad.hideKeyboard();
        MobileElement e27 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[5]/android.widget.EditText");
        Assert.assertEquals(e27.getText(),"20 gr");


    }
}
