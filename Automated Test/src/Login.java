import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class Login {

    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.2.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);


        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement e01 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        e01.click();
        ad.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement e02 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"MoreNav\"]/android.view.ViewGroup");
        e02.click();
        MobileElement e03 = ad.findElementByAccessibilityId("button-AuthHeader-Masuk");
        e03.click();

        //Login with empty email & password
        MobileElement e04 = ad.findElementByAccessibilityId("button-Signin-login");
        e04.click();
        MobileElement e05 = ad.findElementByAccessibilityId("button-Signin-login");
        e05.click();
        MobileElement e06 = ad.findElementByXPath("//*[@text=\"Email harus diisi\"]");
        Assert.assertEquals(e06.getText(),"Email harus diisi");
        MobileElement e07 = ad.findElementByXPath("//*[@text=\"Password harus diisi\"]");
        Assert.assertEquals(e07.getText(),"Password harus diisi");

        //Login without email format
        MobileElement e08 = ad.findElementByAccessibilityId("textfield-Signin-emailInput");
        e08.sendKeys("mjaee6@gmail");
        ad.hideKeyboard();
        MobileElement e09 = ad.findElementByAccessibilityId("textfield-Signin-Password");
        e09.sendKeys("jaenul123");
        ad.hideKeyboard();
        MobileElement e10 = ad.findElementByAccessibilityId("button-Signin-login");
        e10.click();
        MobileElement e11 = ad.findElementByAccessibilityId("button-Signin-login");
        e11.click();
        MobileElement e12 = ad.findElementByXPath("//*[@text=\"Email salah format\"]");
        Assert.assertEquals(e12.getText(),"Email salah format");

        //Login with email and password not registered
        MobileElement e13 = ad.findElementByAccessibilityId("textfield-Signin-emailInput");
        e13.clear();
        MobileElement el4 = ad.findElementByAccessibilityId("textfield-Signin-emailInput");
        el4.sendKeys("mjaee@gmail.com");
        ad.hideKeyboard();
        MobileElement el7 = ad.findElementByAccessibilityId("button-Signin-login");
        el7.click();
        MobileElement el8 = ad.findElementByAccessibilityId("button-Signin-login");
        el8.click();
        MobileElement e19 = ad.findElementByXPath("//*[@text=\"Email belum terdaftar\"]");
        Assert.assertEquals(e19.getText(),"Email belum terdaftar");
        MobileElement e20 = ad.findElementById("android:id/button1");
        e20.click();

        //Login with email valid
        MobileElement e21 = ad.findElementByAccessibilityId("textfield-Signin-emailInput");
        e21.clear();
        MobileElement e22 = ad.findElementByAccessibilityId("textfield-Signin-emailInput");
        e22.sendKeys("mjaee6@gmail.com");
        ad.hideKeyboard();
        MobileElement e25 = ad.findElementByAccessibilityId("button-Signin-login");
        e25.click();
        MobileElement e26 = ad.findElementByAccessibilityId("button-Signin-login");
        e26.click();
        MobileElement e27 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"HomeNav\"]/android.widget.TextView");
        Assert.assertEquals(e27.getText(),"Home");

    }
}


