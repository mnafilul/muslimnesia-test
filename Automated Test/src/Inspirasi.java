import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Inspirasi {
    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.2.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);


        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement el0 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        el0.click();
        MobileElement el1 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el1.click();
        MobileElement el2 = (MobileElement) ad.findElementByAccessibilityId("Touchable-ListSurah-Al-Baqarah");
        el2.click();

        MobileElement el3 = (MobileElement) ad.findElementByAccessibilityId("Touchable-ListAyat-ChangeSuratModal");
        el3.click();
        //ad.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Yaasin\"));");
        MobileElement el4 = (MobileElement) ad.findElementByAccessibilityId("Touchable-ListAyatModal-Al-Maaidah");
        el4.click();
        MobileElement el5 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView");
        el5.click();
        MobileElement el6 = (MobileElement) ad.findElementByAccessibilityId("SearchInput-ListSurah-Surah");
        el6.sendKeys("nas");
        ad.hideKeyboard();
        MobileElement el7 = (MobileElement) ad.findElementByAccessibilityId("Touchable-ListSurah-An-Nas");
        el7.click();
        el7.click();
        MobileElement el8 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView");
        el8.click();
        MobileElement el9 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BackButtonArrow-ListDoa-Back\"]/android.widget.ImageView");
        el9.click();

        //dzikir&doa
        MobileElement el10 = (MobileElement) ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup");
        el10.click();
        MobileElement el11 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BgButton-Inspirasi-Doa\"]/android.view.ViewGroup");
        el11.click();
        MobileElement el12 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BgButton-Doa-Semua\"]/android.view.ViewGroup/android.widget.TextView");
        el12.click();
        MobileElement el13 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-ListDoa-Do’a Menjelang Tidur\"]/android.widget.TextView");
        el13.click();
        MobileElement el14 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BackButtonArrow-DoaDetail-Back\"]/android.widget.ImageView");
        el14.click();
        MobileElement el15 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BackButtonArrow-ListDoa-Back\"]/android.widget.ImageView");
        el15.click();
        el15.click();
        MobileElement el16 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BgButton-Inspirasi-Dzikir\"]/android.view.ViewGroup/android.widget.TextView");
        el16.click();
        MobileElement el17 = (MobileElement) ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BgButton-Dzikir-Fardhu\"]/android.view.ViewGroup/android.widget.TextView");
        el17.click();
    }
}
