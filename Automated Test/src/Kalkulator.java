import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Kalkulator {
    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.2.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);


        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement el0 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        el0.click();
        MobileElement el1 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el1.click();
        MobileElement el2 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el2.click();

        //Zakat Fitrah
        MobileElement el3 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Button-Zakat-ZakatFitrah\"]/android.view.ViewGroup");
        el3.click();
        MobileElement el4 = ad.findElementByAccessibilityId("TextInput-Fitrah-JumlahOrang");
        el4.sendKeys("4");
        ad.hideKeyboard();
        MobileElement el5 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-Fitrah-Hitung\"]/android.widget.TextView");
        el5.click();
        MobileElement el6 = ad.findElementByAccessibilityId("Touchable-Fitrah-Hitung");
        el6.click();
        MobileElement e17 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[2]");
        Assert.assertEquals(e17.getText(),"10 Liter");

        //Input with alphabet
        MobileElement el8 = ad.findElementByAccessibilityId("TextInput-Fitrah-JumlahOrang");
        el8.sendKeys("abc");
        MobileElement el9 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-Fitrah-Hitung\"]/android.widget.TextView");
        el9.click();
        MobileElement e20 = ad.findElementByAccessibilityId("Touchable-Fitrah-Hitung");
        e20.click();
        MobileElement e21 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
        Assert.assertEquals(e21.getText(),"Jumlah orang harus diisi dan harus bernilai lebih dari 0");
        MobileElement e22 = ad.findElementById("android:id/button1");
        e22.click();
        MobileElement e23 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.ImageView");
        e23.click();

        //Qurban
        MobileElement e24 = ad.findElementByXPath("//android.widget.Button[@content-desc=\"QURBAN\"]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView");
        e24.click();
        MobileElement e25 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.support.v4.view.ViewPager/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.Spinner");
        e25.click();
        MobileElement e26 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[1]");
        e26.click();
        MobileElement e27 = ad.findElementByAccessibilityId("TextInput-Qurban-Jumlah");
        e27.sendKeys("2");
        ad.hideKeyboard();
        MobileElement e28 = ad.findElementByAccessibilityId("TextInput-Qurban-LamaTabungan");
        e28.sendKeys("15");
        ad.hideKeyboard();
        MobileElement e29 = ad.findElementByAccessibilityId("Touchable-Qurban-Hitung");
        e29.click();
        MobileElement e30 = ad.findElementByAccessibilityId("Touchable-Qurban-Hitung");
        e30.click();
        MobileElement e31 = ad.findElementByAccessibilityId("TextInput-Qurban-Jumlah");
        e31.sendKeys("0");
        ad.hideKeyboard();
        MobileElement e32 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
        Assert.assertEquals(e32.getText(),"Jumlah qurban harus diisi dan harus bernilai lebih dari 0");


    }
}
