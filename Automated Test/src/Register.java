import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Register {
    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.2.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);


        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement el0 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        el0.click();
        ad.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        MobileElement el1 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"MoreNav\"]/android.widget.TextView");
        el1.click();
        MobileElement el2 = ad.findElementByAccessibilityId("button-AuthHeader-Daftar");
        el2.click();

        //Register with empty form
        MobileElement e13 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        e13.click();
        MobileElement e14 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        e14.click();
        MobileElement e15 = ad.findElementByXPath("//*[@text=\"Nama harus diisi\"]");
        Assert.assertEquals(e15.getText(),"Nama harus diisi");

        //Register with not formatted email
        MobileElement el6 = ad.findElementByAccessibilityId("InputText-SignupFirst-Name");
        el6.sendKeys("John Doe");
        ad.hideKeyboard();
        MobileElement el7 = ad.findElementByAccessibilityId("InputText-SignupFirst-Email");
        el7.sendKeys("mnafilul@gmail");
        ad.hideKeyboard();
        MobileElement e18 = ad.findElementByAccessibilityId("InputText-SignupFirst-Password");
        e18.sendKeys("rahasia123");
        ad.hideKeyboard();
        MobileElement el9 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        el9.click();
        MobileElement buttondaftar1 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        buttondaftar1.click();
        MobileElement e20 = ad.findElementByXPath("//*[@text=\"E-mail salah format\"]");
        Assert.assertEquals(e20.getText(),"E-mail salah format");



        // Register with valid input
        MobileElement e21 = ad.findElementByAccessibilityId("InputText-SignupFirst-Name");
        e21.sendKeys("John Doe");
        ad.hideKeyboard();
        MobileElement e22 = ad.findElementByAccessibilityId("InputText-SignupFirst-Email");
        e22.sendKeys("mnafilul@gmail.com");
        ad.hideKeyboard();
        MobileElement e23 = ad.findElementByAccessibilityId("InputText-SignupFirst-Password");
        e23.sendKeys("rahasia123");
        ad.hideKeyboard();
        MobileElement e24 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        e24.click();
        MobileElement e25 = ad.findElementByAccessibilityId("InputText-SignupFirst-Daftar");
        e25.click();
        MobileElement e26 = ad.findElementByXPath("//*[@text=\"Selamat datang di Muslimnesia\"]");
        Assert.assertEquals(e26.getText(),"Selamat datang di Muslimnesia");
        MobileElement e27 = ad.findElementByAccessibilityId("InputText-SignupSecond-TanggalLahir");
        e27.click();
        MobileElement e28 = ad.findElementByAccessibilityId("22 Januari 1990");
        e28.click();
        MobileElement e29 = ad.findElementById("android:id/button1");
        e29.click();
        MobileElement e30 = ad.findElementByAccessibilityId("InputText-SignupSecond-Gender");
        e30.click();
        MobileElement e31 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.CheckedTextView[2]");
        e31.click();
        MobileElement e32 = ad.findElementByAccessibilityId("Button-SignupSecond-Next");
        e32.click();
//        MobileElement e26 = ad.findElementByAccessibilityId("Button-SignupSecond-Next");
//        e26.click();
        MobileElement e33 = ad.findElementByXPath("//*[@text=\"Atur foto profil kamu\"]");
        Assert.assertEquals(e33.getText(),"Atur foto profil kamu");
        MobileElement e34 = ad.findElementByAccessibilityId("Touchable-SignupThird-Simpan");
        e34.click();
        MobileElement e35 = ad.findElementByAccessibilityId("Touchable-SignupThird-Simpan");
        e35.click();
    }
}
