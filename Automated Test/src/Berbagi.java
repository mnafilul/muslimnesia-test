import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.offset.ElementOption.element;

public class Berbagi {

    public static void main(String args[]) throws MalformedURLException {

        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.DEVICE_NAME, "2764d01a7d04");
        dc.setCapability("platformName", "android");
        dc.setCapability(MobileCapabilityType.APP, "E:\\Muslimnesia\\Muslimnesia-1.11.2.apk");
        dc.setCapability("appPackage", "org.dot.nafis.muslimnesia");
        dc.setCapability("appActivity", ".MainActivity");

        AndroidDriver<AndroidElement> ad = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);

        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        MobileElement el0 = ad.findElementById("com.android.packageinstaller:id/permission_allow_button");
        el0.click();
        //Filter
        MobileElement e01 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BerbagiNav\"]/android.widget.TextView");
        e01.click();
        MobileElement e02 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-ProductListHeader-Kategori\"]/android.widget.TextView");
        e02.click();
        MobileElement e03 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.ImageView");
        e03.click();
        MobileElement e04 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.ImageView");
        e04.click();
        MobileElement e05 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup");
        e05.click();
        MobileElement e06 = ad.findElementByXPath("(//android.view.ViewGroup[@content-desc=\"Touchable-ProductItem-Product\"])[1]/android.widget.TextView[1]");
        e06.click();
        ad.hideKeyboard();
        ad.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\"Deskripsi\"));");
        MobileElement e07 = ad.findElementByAccessibilityId("Touchable-DetailProduct-Beli");
        e07.click();
        

        //transaksi
        ad.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        MobileElement el1 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"BerbagiNav\"]/android.widget.TextView");
//        el1.click();
//        MobileElement el2 = ad.findElementByXPath("(//android.view.ViewGroup[@content-desc=\"Touchable-ProductItem-Product\"])[1]");
//        el2.click();
//        MobileElement el3 = ad.findElementByAccessibilityId("Touchable-DetailProduct-Beli");
//        el3.click();
        MobileElement el4 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[8]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView");
        el4.click();
        MobileElement el5 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ListView/android.widget.LinearLayout[4]/android.widget.LinearLayout/android.widget.TextView[2]");
        el5.click();
        MobileElement el6 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el6.click();
        MobileElement el7 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView");
        el7.click();
        MobileElement el8 = ad.findElementByAccessibilityId("Touchable-DetailProduct-Beli");
        el8.click();
        MobileElement el9 = ad.findElementByXPath("(//android.view.ViewGroup[@content-desc=\"CheckBox-CartItem-CheckBox\"])[1]");
        el9.click();

        //checkout
        MobileElement el10 = ad.findElementByAccessibilityId("Touchable-CartFooter-CheckOut");
        el10.click();

        //voucher
        MobileElement checkbox1 = ad.findElementByAccessibilityId("CheckBox-VoucherInput-CheckBox");
        checkbox1.click();
        MobileElement el91 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]");
        el91.sendKeys("GRATISDONG");
        ad.hideKeyboard();
        MobileElement el92 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]");
        el92.click();
//        MobileElement e193 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]");
//        Assert.assertEquals(e193.getText(),"Kode voucher berhasil digunakan");
        MobileElement el94 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.EditText");
        el94.sendKeys("Aku pesen ini ya");
        ad.hideKeyboard();

        //Pembayaran
//        MobileElement el95 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView");
//        el95.click();
        MobileElement el11 = ad.findElementByAccessibilityId("Touchable-CheckOut-LanjutKePembayaran");
        el11.click();
        MobileElement el12 = ad.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[5]/android.widget.ListView/android.view.View[2]/android.view.View");
        el12.click();
        MobileElement el13 = ad.findElementById("btnCancel_123");
        el13.click();
        MobileElement el14 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-DetailPembelian-Back\"]/android.widget.ImageView");
        el14.click();
        MobileElement el15 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-PembelianToolbar-ListPembelian\"]/android.widget.ImageView");
        el15.click();
        MobileElement el16 = ad.findElementByXPath("(//android.view.ViewGroup[@content-desc=\"Touchable-PembelianItem-Item\"])[1]");
        el16.click();
        MobileElement el17 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-DetailPembelian-Back\"]/android.widget.ImageView");
        el17.click();
        MobileElement el18 = ad.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Touchable-ListPembalian-Back\"]/android.widget.ImageView");
        el18.click();







    }
}
